#!/bin/bash

set -e

APP_PATH=/tmp/app.zip

if [ -e "$APP_PATH" ]; then
    echo 'App already installed, skipping installation'
else
    echo "REPO_URL: $REPO_URL"
    echo "REPO_NAME: $REPO_NAME"
    echo "ARTIFACT_TYPE: $ARTIFACT_TYPE"
    echo "ARTIFACT_GROUP: $ARTIFACT_GROUP"
    echo "ARTIFACT_ID: $ARTIFACT_ID"
    echo "ARTIFACT_VERSION: $ARTIFACT_VERSION"

    echo "Retrieving artifact informations"
    #TODO Later store env type of artifact in version and not in zip name.
    # If we are in hotfix or master env
    if [ "$ARTIFACT_TYPE" = "master" ] || [ "$ARTIFACT_TYPE" = "hotfix" ]; then
        ARCHIVE=$(curl -u $REPO_USERNAME:$REPO_PASSWORD -G $REPO_URL/api/storage/$REPO_NAME/${ARTIFACT_GROUP//[.]/\/}/$ARTIFACT_ID/$ARTIFACT_VERSION | jq -r '.children[0].uri')
    else
        ARCHIVE=$(curl -u $REPO_USERNAME:$REPO_PASSWORD -G $REPO_URL/api/storage/$REPO_NAME/${ARTIFACT_GROUP//[.]/\/}/$ARTIFACT_ID/$ARTIFACT_VERSION | jq -r '.children | [.[] | select(.uri | contains(env.ARTIFACT_TYPE)) | .uri][0]')
    fi
    echo 'Found archive:' $ARCHIVE

    URL=$REPO_URL/$REPO_NAME/${ARTIFACT_GROUP//[.]/\/}/$ARTIFACT_ID/$ARTIFACT_VERSION$ARCHIVE

    echo 'Download artifact at:' $URL
    curl -u $REPO_USERNAME:$REPO_PASSWORD $URL --output $APP_PATH

    echo 'Installing artifact'
    rm -rf /usr/share/nginx/html/*
    unzip $APP_PATH -d /usr/share/nginx/html/

    echo 'Add reverse proxies for Api & Oauth'
    cat > /etc/nginx/nginx.conf <<EOF
user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;
    log_format  main  '\$remote_addr - \$remote_user [\$time_local] "\$request" '
                      '\$status \$body_bytes_sent "\$http_referer" '
                      '"\$http_user_agent" "\$http_x_forwarded_for"';
    access_log  /var/log/nginx/access.log  main;
    sendfile        on;
    keepalive_timeout  65;

    server {
        listen       80;

        location / {
            root   /usr/share/nginx/html;
            index  index.html;
        }
EOF
    echo -e "$NGINX_ADDITIONNAL_CONF" >> /etc/nginx/nginx.conf
    cat >> /etc/nginx/nginx.conf <<EOF
    }
}
EOF
fi
more /etc/nginx/nginx.conf
echo 'Launching Nginx'
nginx -g 'daemon off;'