#Description
This image will download the latest web artifact (zipped) from Artifactory generic repository and unzip it in Nginx html root

#Usages:
* REPO_URL=http://my-artifactory.com/artifactory
* REPO_USERNAME=username
* REPO_PASSWORD=password
* REPO_NAME=web-snapshot-local
* ARTIFACT_TYPE=develop (matching string to find in artifact name, if there is multiple different version of snapshot)
* ARTIFACT_GROUP=com.enterprise
* ARTIFACT_ID=name
* ARTIFACT_VERSION=2.2.6.SNAPSHOT
* NGINX_ADDITIONNAL_CONF="location /oauth/ { proxy_pass http://feature.arxcf.com/oauth/; }\nlocation /services/ { proxy_pass http://feature.arxcf.com/services/; }"